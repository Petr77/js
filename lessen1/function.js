/*
2.Напишите функцию sumTo(n), которая для данного n вычисляет сумму чисел от 1 до n, 
например: sumTo(3) = 1 + 2 + 3, sumTo(2) = 1 + 2 и т.п.

Решите задачу двумя способами (рекурсиеи? и с помощью циклов). Какой способ, по-вашему, лучше?*/

var sum = 0;

function sumTo(n){
	if (n > 0) {
		for (var i = 0; i <= n; i++){
			var	sum = sum + i;
		}
	}
}
//sumTo(7);

console.log();

function sumToRec(n){
	if ( n == 1 ) {
		return n;
	}else{
		return n + sumToRec( n-1 );
	}
}
var t =sumToRec(9);
console.log(t);