var studentsAndPoints = ['Алексей Петров', 0,
						 'Ирина Овчинникова', 60,
						 'Глеб Стукалов', 30,
						 'Антон Павлович', 30, 
						 'Виктория Заровская', 30, 
						 'Алексей Левенец', 70, 
						 'Тимур Вамуш', 30, 
						 'Евгений Прочан', 60, 
						 'Александр Малов', 0];



/*Создать два массива students и points
и заполнить их данными из studentsAndPoints без использования циклов.
В первом массиве фамилии студентов, во втором баллы. Индексы совпадают.
*/

 var points = studentsAndPoints.filter(function(number){
 	return number >= 0;
 }); 

console.log(points);

var students = studentsAndPoints.filter(function(name){
 	return typeof(name) === "string";
 }); 

console.log(students); 

/*
Вывести список студентов без использования циклов в следующем виде:
*/

students.forEach(function(name, i){
	console.log(name, points[i]);
}) 

/*
3) Найти студента набравшего наибольшее количество баллов, и вывести информацию в формате:
*/

var maxPoints = Math.max.apply ( Math , points );
points.forEach ( function ( bal , i ) {
  if ( bal === maxPoints ){
    console.log ( 'Студент набравший максимальный балл: ' + students[i] + ' ( ' + maxPoints + ' баллов ) ')
  }
} );



//4) Используя метод массива map увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30.

points = points.map ( function ( bal, i ) {
  if ( students[i] === 'Ирина Овчинникова' || students[i] === 'Александр Малов' ) { 
    return bal += 30; 
 }
  else {
    return bal;
  }
     
});
console.log("Увеличили баллы на 30 двум студентам")
console.log(points)

 


/*for (var i=0, len = studentsAndPoints.length; i < len; i++ ) {
	console.log(studentsAndPoints[i]);
}*/